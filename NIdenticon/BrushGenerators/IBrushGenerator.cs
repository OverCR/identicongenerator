﻿using System.Drawing;

namespace IdenticonLib.BrushGenerators {
    public interface IBrushGenerator {
        Brush GetBrush(uint seed);
    }
}

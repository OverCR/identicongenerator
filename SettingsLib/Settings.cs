﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;

namespace SettingsLib {
  public class Settings : Dictionary<string, string> {
    private XDocument xDocument;
    private string filePath;
    private string elementName;

    public Settings(string filePath, string rootName = "mainSettings", string elementName = "setting") {
      if (!File.Exists(filePath)) {
        File.Create(filePath).Dispose();

        using (StreamWriter sw = new StreamWriter(filePath)) {
          sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
          sw.WriteLine("<{0}>", rootName);
          sw.WriteLine("</{0}>", rootName);

          sw.Flush();
          sw.Close();
          sw.Dispose();
        }
      }

      xDocument = XDocument.Load(filePath);
      this.filePath = filePath;
      this.elementName = elementName;

      foreach (var keyElement in xDocument.Descendants(elementName)) {
        this.Add(keyElement.Attribute("name").Value, keyElement.Value);
      }
    }

    public new string this[string key] {
      get {
        if (base.ContainsKey(key)) {
          return base[key];
        } else {
          return null;
        }
      }

      set {
        if (base.ContainsKey(key)) {
          base[key] = value;
          xDocument.XPathSelectElement(string.Format("//setting[@name='{0}']", key)).SetValue(value);
        } else {
          base[key] = value;

          var xElement = new XElement(elementName);
          xElement.SetAttributeValue("name", key);
          xElement.SetValue(value);

          xDocument.Root.Add(xElement);
        }
        xDocument.Save(filePath);
      }
    }
  }
}
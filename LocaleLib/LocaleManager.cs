﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace LocaleLib {
  public class LocaleManager {
    // TODO: Refactor, use XPath
    private Dictionary<string, Locale> availableLocales;
    private string activeLocale;
    private XDocument activeLocaleDoc;

    /// <summary>
    /// Returns friendly name (e.g 'English (US)') specified in locale XML.
    /// </summary>
    public string ActiveLocaleFriendlyName {
      get { return availableLocales[activeLocale].FriendlyName; }
    }

    /// <summary>
    /// Returns active locale identificator (e.g 'en_US').
    /// </summary>
    public string ActiveLocale {
      get { return activeLocale; }
      set {
        if (availableLocales.ContainsKey(value)) {
          activeLocale = value;
          activeLocaleDoc = XDocument.Load(availableLocales[activeLocale].FilePath);
        } else {
          foreach (KeyValuePair<string, Locale> kvp in availableLocales) {
            if (kvp.Value.FriendlyName == value) {
              activeLocale = kvp.Key;
              activeLocaleDoc = XDocument.Load(availableLocales[activeLocale].FilePath);
              break;
            }
          }
        }
      }
    }

    /// <summary>
    /// Returns dictionary of currently available locales.
    /// </summary>
    public Dictionary<string, Locale> AvailableLocales {
      get { return availableLocales; }
    }

    /// <summary>
    /// Creates a new LocaleManager object.
    /// </summary>
    /// <param name="directory">Absolute or relative directory path containing locale files.</param>
    /// <param name="defaultLocale">Default locale, that LocaleManager starts with. Defaults to en_US.</param>
    public LocaleManager(string directory, string defaultLocale = "en_US") {
      RescanLocales(directory);

      if (availableLocales.ContainsKey(defaultLocale)) {
        this.activeLocale = defaultLocale;
        activeLocaleDoc = XDocument.Load(availableLocales[activeLocale].FilePath);
      } else {
        this.activeLocale = availableLocales.Keys.FirstOrDefault();
        activeLocaleDoc = XDocument.Load(availableLocales[activeLocale].FilePath);
      }
    }

    /// <summary>
    /// Searches for localized string in locale using specified pattern.
    /// </summary>
    /// <param name="pattern">Pattern to search for.</param>
    /// <returns>Localized string if found, else - returns queried pattern.</returns>
    public string GetLocalizedString(string pattern) {
      foreach (XElement keyElement in activeLocaleDoc.Descendants("localeString")) {
        if (keyElement.Attribute(XName.Get("key")).Value == pattern) {
          return (string)keyElement;
        }
      }
      return pattern;
    }

    /// <summary>
    /// Scans for new locales in specified directory using default file pattern (*.locale.xml).
    /// </summary>
    /// <param name="directory">Directory to search for locale files.</param>
    public void RescanLocales(string directory) {
      FileInfo[] files = new DirectoryInfo(directory).GetFiles("*.locale.xml");
      List<string> availableLocaleFiles = new List<string>();

      availableLocales = new Dictionary<string, Locale>();

      foreach (FileInfo f in files) {
        try {
          availableLocaleFiles.Add(f.FullName);
        } catch { }
      }

      if (availableLocaleFiles.Count == 0) {
        throw new InvalidOperationException("No files found.");
      }

      foreach (string s in availableLocaleFiles) {
        string fileName = Path.GetFileName(s);
        string abbrev = fileName.Split('.')[0];

        if (!availableLocales.ContainsKey(abbrev)) {
          Locale loc = new Locale();
          loc.FilePath = s;
          loc.FriendlyName = GetFriendlyName(s);
          availableLocales.Add(abbrev, loc);
        } else {
          throw new InvalidOperationException("Locale already in here. Case-sensitive file system? Shit happens.");
        }
      }
    }

    /// <summary>
    /// Scans for new locales in specified directory using custom file pattern.
    /// </summary>
    /// <param name="directory">Directory to search for locale files.</param>
    /// <param name="filePattern">Custom file pattern to match (e.g. *.loc.xml)</param>
    public void RescanLocales(string directory, string filePattern) {
      FileInfo[] files = new DirectoryInfo(directory).GetFiles(filePattern);
      List<string> availableLocaleFiles = new List<string>();

      availableLocales = new Dictionary<string, Locale>();

      foreach (FileInfo f in files) {
        try {
          availableLocaleFiles.Add(f.FullName);
        } catch { }
      }

      if (availableLocaleFiles.Count == 0) {
        throw new InvalidOperationException("No files found.");
      }

      foreach (string s in availableLocaleFiles) {
        string fileName = Path.GetFileName(s);
        string abbrev = fileName.Split('.')[0];

        if (!availableLocales.ContainsKey(abbrev)) {
          Locale loc = new Locale();
          loc.FilePath = s;
          loc.FriendlyName = GetFriendlyName(s);
          availableLocales.Add(abbrev, loc);
        } else {
          throw new InvalidOperationException("Locale already in here. Case-sensitive file system? Shit happens.");
        }
      }
    }

    private string GetFriendlyName(string fileName) {
      XDocument xdoc = XDocument.Load(fileName);
      XElement element = xdoc.Descendants("locale").First();
      return element.Attribute(XName.Get("friendlyName")).Value;
    }
  }
}

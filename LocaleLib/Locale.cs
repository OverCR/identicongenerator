﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LocaleLib {
  public struct Locale {
    public string FilePath;
    public string FriendlyName;
  }
}

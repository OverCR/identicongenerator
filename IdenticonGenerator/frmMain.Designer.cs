﻿namespace IdenticonGeneration {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
          this.menuStripMain = new System.Windows.Forms.MenuStrip();
          this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.saveIdenticonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.selectlanguageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.transformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.rotate90DegCCWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.rotate90DegCWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.groupBoxProperties = new System.Windows.Forms.GroupBox();
          this.checkBoxColorFromText = new System.Windows.Forms.CheckBox();
          this.pictureBoxForegroundColorPreview = new System.Windows.Forms.PictureBox();
          this.buttonForegroundColor = new System.Windows.Forms.Button();
          this.checkBoxBackgroundTransparent = new System.Windows.Forms.CheckBox();
          this.pictureBoxBackgroundColorPreview = new System.Windows.Forms.PictureBox();
          this.buttonBackgroundColor = new System.Windows.Forms.Button();
          this.groupBoxBlocks = new System.Windows.Forms.GroupBox();
          this.numericUpDownBlocksVertical = new System.Windows.Forms.NumericUpDown();
          this.labelVert = new System.Windows.Forms.Label();
          this.numericUpDownBlocksHorizontal = new System.Windows.Forms.NumericUpDown();
          this.labelHoriz = new System.Windows.Forms.Label();
          this.groupBoxSize = new System.Windows.Forms.GroupBox();
          this.numericUpDownHeight = new System.Windows.Forms.NumericUpDown();
          this.numericUpDownWidth = new System.Windows.Forms.NumericUpDown();
          this.labelHeight = new System.Windows.Forms.Label();
          this.labelWidth = new System.Windows.Forms.Label();
          this.labelAlgorithm = new System.Windows.Forms.Label();
          this.comboBoxAlgorithm = new System.Windows.Forms.ComboBox();
          this.textBoxSourceText = new System.Windows.Forms.TextBox();
          this.groupBoxIdenticonPreview = new System.Windows.Forms.GroupBox();
          this.pictureBoxIdenticon = new System.Windows.Forms.PictureBox();
          this.buttonGenerate = new System.Windows.Forms.Button();
          this.colorDialogBackground = new System.Windows.Forms.ColorDialog();
          this.saveFileDialogIdenticon = new System.Windows.Forms.SaveFileDialog();
          this.colorDialogForeground = new System.Windows.Forms.ColorDialog();
          this.labelTextInfo = new System.Windows.Forms.Label();
          this.menuStripMain.SuspendLayout();
          this.groupBoxProperties.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxForegroundColorPreview)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBackgroundColorPreview)).BeginInit();
          this.groupBoxBlocks.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlocksVertical)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlocksHorizontal)).BeginInit();
          this.groupBoxSize.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHeight)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWidth)).BeginInit();
          this.groupBoxIdenticonPreview.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIdenticon)).BeginInit();
          this.SuspendLayout();
          // 
          // menuStripMain
          // 
          this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.transformToolStripMenuItem,
            this.helpToolStripMenuItem});
          this.menuStripMain.Location = new System.Drawing.Point(0, 0);
          this.menuStripMain.Name = "menuStripMain";
          this.menuStripMain.Size = new System.Drawing.Size(393, 24);
          this.menuStripMain.TabIndex = 0;
          this.menuStripMain.Text = "menuStrip1";
          // 
          // fileToolStripMenuItem
          // 
          this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveIdenticonToolStripMenuItem,
            this.selectlanguageToolStripMenuItem,
            this.exitToolStripMenuItem});
          this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
          this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
          this.fileToolStripMenuItem.Text = "&File";
          // 
          // saveIdenticonToolStripMenuItem
          // 
          this.saveIdenticonToolStripMenuItem.Name = "saveIdenticonToolStripMenuItem";
          this.saveIdenticonToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
          this.saveIdenticonToolStripMenuItem.Text = "&Save Identicon";
          this.saveIdenticonToolStripMenuItem.Click += new System.EventHandler(this.saveIdenticonToolStripMenuItem_Click);
          // 
          // selectlanguageToolStripMenuItem
          // 
          this.selectlanguageToolStripMenuItem.Name = "selectlanguageToolStripMenuItem";
          this.selectlanguageToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
          this.selectlanguageToolStripMenuItem.Text = "Select &language";
          this.selectlanguageToolStripMenuItem.Click += new System.EventHandler(this.selectlanguageToolStripMenuItem_Click);
          // 
          // exitToolStripMenuItem
          // 
          this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
          this.exitToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
          this.exitToolStripMenuItem.Text = "E&xit";
          this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
          // 
          // transformToolStripMenuItem
          // 
          this.transformToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rotate90DegCCWToolStripMenuItem,
            this.rotate90DegCWToolStripMenuItem});
          this.transformToolStripMenuItem.Name = "transformToolStripMenuItem";
          this.transformToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
          this.transformToolStripMenuItem.Text = "&Transform";
          // 
          // rotate90DegCCWToolStripMenuItem
          // 
          this.rotate90DegCCWToolStripMenuItem.Name = "rotate90DegCCWToolStripMenuItem";
          this.rotate90DegCCWToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
          this.rotate90DegCCWToolStripMenuItem.Text = "Rotate 90° C&CW";
          this.rotate90DegCCWToolStripMenuItem.Click += new System.EventHandler(this.rotate90DegCCWToolStripMenuItem_Click);
          // 
          // rotate90DegCWToolStripMenuItem
          // 
          this.rotate90DegCWToolStripMenuItem.Name = "rotate90DegCWToolStripMenuItem";
          this.rotate90DegCWToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
          this.rotate90DegCWToolStripMenuItem.Text = "Rotate 90° C&W";
          this.rotate90DegCWToolStripMenuItem.Click += new System.EventHandler(this.rotate90DegCWToolStripMenuItem_Click);
          // 
          // helpToolStripMenuItem
          // 
          this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
          this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
          this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
          this.helpToolStripMenuItem.Text = "&Help";
          // 
          // aboutToolStripMenuItem
          // 
          this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
          this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
          this.aboutToolStripMenuItem.Text = "&About";
          this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
          // 
          // groupBoxProperties
          // 
          this.groupBoxProperties.Controls.Add(this.checkBoxColorFromText);
          this.groupBoxProperties.Controls.Add(this.pictureBoxForegroundColorPreview);
          this.groupBoxProperties.Controls.Add(this.buttonForegroundColor);
          this.groupBoxProperties.Controls.Add(this.checkBoxBackgroundTransparent);
          this.groupBoxProperties.Controls.Add(this.pictureBoxBackgroundColorPreview);
          this.groupBoxProperties.Controls.Add(this.buttonBackgroundColor);
          this.groupBoxProperties.Controls.Add(this.groupBoxBlocks);
          this.groupBoxProperties.Controls.Add(this.groupBoxSize);
          this.groupBoxProperties.Controls.Add(this.labelAlgorithm);
          this.groupBoxProperties.Controls.Add(this.comboBoxAlgorithm);
          this.groupBoxProperties.Location = new System.Drawing.Point(4, 29);
          this.groupBoxProperties.Name = "groupBoxProperties";
          this.groupBoxProperties.Size = new System.Drawing.Size(118, 275);
          this.groupBoxProperties.TabIndex = 1;
          this.groupBoxProperties.TabStop = false;
          this.groupBoxProperties.Text = "Identicon properties";
          // 
          // checkBoxColorFromText
          // 
          this.checkBoxColorFromText.AutoSize = true;
          this.checkBoxColorFromText.Location = new System.Drawing.Point(7, 255);
          this.checkBoxColorFromText.Name = "checkBoxColorFromText";
          this.checkBoxColorFromText.RightToLeft = System.Windows.Forms.RightToLeft.No;
          this.checkBoxColorFromText.Size = new System.Drawing.Size(69, 17);
          this.checkBoxColorFromText.TabIndex = 13;
          this.checkBoxColorFromText.Text = "From text";
          this.checkBoxColorFromText.UseVisualStyleBackColor = true;
          this.checkBoxColorFromText.CheckedChanged += new System.EventHandler(this.checkBoxColorFromText_CheckedChanged);
          // 
          // pictureBoxForegroundColorPreview
          // 
          this.pictureBoxForegroundColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pictureBoxForegroundColorPreview.Location = new System.Drawing.Point(84, 231);
          this.pictureBoxForegroundColorPreview.Name = "pictureBoxForegroundColorPreview";
          this.pictureBoxForegroundColorPreview.Size = new System.Drawing.Size(28, 22);
          this.pictureBoxForegroundColorPreview.TabIndex = 12;
          this.pictureBoxForegroundColorPreview.TabStop = false;
          // 
          // buttonForegroundColor
          // 
          this.buttonForegroundColor.Location = new System.Drawing.Point(6, 230);
          this.buttonForegroundColor.Name = "buttonForegroundColor";
          this.buttonForegroundColor.Size = new System.Drawing.Size(75, 24);
          this.buttonForegroundColor.TabIndex = 11;
          this.buttonForegroundColor.TabStop = false;
          this.buttonForegroundColor.Text = "Foreground";
          this.buttonForegroundColor.UseVisualStyleBackColor = true;
          this.buttonForegroundColor.Click += new System.EventHandler(this.buttonForegroundColor_Click);
          // 
          // checkBoxBackgroundTransparent
          // 
          this.checkBoxBackgroundTransparent.AutoSize = true;
          this.checkBoxBackgroundTransparent.Location = new System.Drawing.Point(7, 207);
          this.checkBoxBackgroundTransparent.Name = "checkBoxBackgroundTransparent";
          this.checkBoxBackgroundTransparent.Size = new System.Drawing.Size(83, 17);
          this.checkBoxBackgroundTransparent.TabIndex = 10;
          this.checkBoxBackgroundTransparent.Text = "Transparent";
          this.checkBoxBackgroundTransparent.UseVisualStyleBackColor = true;
          this.checkBoxBackgroundTransparent.CheckedChanged += new System.EventHandler(this.checkBoxBackgroundTransparent_CheckedChanged);
          // 
          // pictureBoxBackgroundColorPreview
          // 
          this.pictureBoxBackgroundColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pictureBoxBackgroundColorPreview.Location = new System.Drawing.Point(84, 183);
          this.pictureBoxBackgroundColorPreview.Name = "pictureBoxBackgroundColorPreview";
          this.pictureBoxBackgroundColorPreview.Size = new System.Drawing.Size(28, 22);
          this.pictureBoxBackgroundColorPreview.TabIndex = 9;
          this.pictureBoxBackgroundColorPreview.TabStop = false;
          // 
          // buttonBackgroundColor
          // 
          this.buttonBackgroundColor.Location = new System.Drawing.Point(6, 182);
          this.buttonBackgroundColor.Name = "buttonBackgroundColor";
          this.buttonBackgroundColor.Size = new System.Drawing.Size(75, 24);
          this.buttonBackgroundColor.TabIndex = 5;
          this.buttonBackgroundColor.TabStop = false;
          this.buttonBackgroundColor.Text = "Background";
          this.buttonBackgroundColor.UseVisualStyleBackColor = true;
          this.buttonBackgroundColor.Click += new System.EventHandler(this.buttonBackgroundColor_Click);
          // 
          // groupBoxBlocks
          // 
          this.groupBoxBlocks.Controls.Add(this.numericUpDownBlocksVertical);
          this.groupBoxBlocks.Controls.Add(this.labelVert);
          this.groupBoxBlocks.Controls.Add(this.numericUpDownBlocksHorizontal);
          this.groupBoxBlocks.Controls.Add(this.labelHoriz);
          this.groupBoxBlocks.Location = new System.Drawing.Point(6, 121);
          this.groupBoxBlocks.Name = "groupBoxBlocks";
          this.groupBoxBlocks.Size = new System.Drawing.Size(106, 57);
          this.groupBoxBlocks.TabIndex = 5;
          this.groupBoxBlocks.TabStop = false;
          this.groupBoxBlocks.Text = "Blocks";
          // 
          // numericUpDownBlocksVertical
          // 
          this.numericUpDownBlocksVertical.Location = new System.Drawing.Point(55, 30);
          this.numericUpDownBlocksVertical.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
          this.numericUpDownBlocksVertical.Name = "numericUpDownBlocksVertical";
          this.numericUpDownBlocksVertical.Size = new System.Drawing.Size(45, 20);
          this.numericUpDownBlocksVertical.TabIndex = 4;
          this.numericUpDownBlocksVertical.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
          this.numericUpDownBlocksVertical.ValueChanged += new System.EventHandler(this.numericUpDownBlocksVertical_ValueChanged);
          // 
          // labelVert
          // 
          this.labelVert.AutoSize = true;
          this.labelVert.Location = new System.Drawing.Point(54, 16);
          this.labelVert.Name = "labelVert";
          this.labelVert.Size = new System.Drawing.Size(45, 13);
          this.labelVert.TabIndex = 6;
          this.labelVert.Text = "Vertical:";
          // 
          // numericUpDownBlocksHorizontal
          // 
          this.numericUpDownBlocksHorizontal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.numericUpDownBlocksHorizontal.Location = new System.Drawing.Point(6, 30);
          this.numericUpDownBlocksHorizontal.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
          this.numericUpDownBlocksHorizontal.Name = "numericUpDownBlocksHorizontal";
          this.numericUpDownBlocksHorizontal.Size = new System.Drawing.Size(45, 20);
          this.numericUpDownBlocksHorizontal.TabIndex = 3;
          this.numericUpDownBlocksHorizontal.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
          this.numericUpDownBlocksHorizontal.ValueChanged += new System.EventHandler(this.numericUpDownBlocksHorizontal_ValueChanged);
          // 
          // labelHoriz
          // 
          this.labelHoriz.AutoSize = true;
          this.labelHoriz.Location = new System.Drawing.Point(5, 16);
          this.labelHoriz.Name = "labelHoriz";
          this.labelHoriz.Size = new System.Drawing.Size(37, 13);
          this.labelHoriz.TabIndex = 7;
          this.labelHoriz.Text = "Horiz.:";
          // 
          // groupBoxSize
          // 
          this.groupBoxSize.Controls.Add(this.numericUpDownHeight);
          this.groupBoxSize.Controls.Add(this.numericUpDownWidth);
          this.groupBoxSize.Controls.Add(this.labelHeight);
          this.groupBoxSize.Controls.Add(this.labelWidth);
          this.groupBoxSize.Location = new System.Drawing.Point(6, 60);
          this.groupBoxSize.Name = "groupBoxSize";
          this.groupBoxSize.Size = new System.Drawing.Size(106, 57);
          this.groupBoxSize.TabIndex = 4;
          this.groupBoxSize.TabStop = false;
          this.groupBoxSize.Text = "Size";
          // 
          // numericUpDownHeight
          // 
          this.numericUpDownHeight.Location = new System.Drawing.Point(55, 30);
          this.numericUpDownHeight.Maximum = new decimal(new int[] {
            8192,
            0,
            0,
            0});
          this.numericUpDownHeight.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
          this.numericUpDownHeight.Name = "numericUpDownHeight";
          this.numericUpDownHeight.Size = new System.Drawing.Size(45, 20);
          this.numericUpDownHeight.TabIndex = 2;
          this.numericUpDownHeight.Value = new decimal(new int[] {
            256,
            0,
            0,
            0});
          this.numericUpDownHeight.ValueChanged += new System.EventHandler(this.numericUpDownHeight_ValueChanged);
          // 
          // numericUpDownWidth
          // 
          this.numericUpDownWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.numericUpDownWidth.Location = new System.Drawing.Point(6, 30);
          this.numericUpDownWidth.Maximum = new decimal(new int[] {
            8192,
            0,
            0,
            0});
          this.numericUpDownWidth.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
          this.numericUpDownWidth.Name = "numericUpDownWidth";
          this.numericUpDownWidth.Size = new System.Drawing.Size(45, 20);
          this.numericUpDownWidth.TabIndex = 1;
          this.numericUpDownWidth.Value = new decimal(new int[] {
            256,
            0,
            0,
            0});
          this.numericUpDownWidth.ValueChanged += new System.EventHandler(this.numericUpDownWidth_ValueChanged);
          // 
          // labelHeight
          // 
          this.labelHeight.AutoSize = true;
          this.labelHeight.Location = new System.Drawing.Point(54, 16);
          this.labelHeight.Name = "labelHeight";
          this.labelHeight.Size = new System.Drawing.Size(41, 13);
          this.labelHeight.TabIndex = 6;
          this.labelHeight.Text = "Height:";
          // 
          // labelWidth
          // 
          this.labelWidth.AutoSize = true;
          this.labelWidth.Location = new System.Drawing.Point(5, 16);
          this.labelWidth.Name = "labelWidth";
          this.labelWidth.Size = new System.Drawing.Size(38, 13);
          this.labelWidth.TabIndex = 7;
          this.labelWidth.Text = "Width:";
          // 
          // labelAlgorithm
          // 
          this.labelAlgorithm.AutoSize = true;
          this.labelAlgorithm.Location = new System.Drawing.Point(5, 20);
          this.labelAlgorithm.Name = "labelAlgorithm";
          this.labelAlgorithm.Size = new System.Drawing.Size(53, 13);
          this.labelAlgorithm.TabIndex = 1;
          this.labelAlgorithm.Text = "Algorithm:";
          // 
          // comboBoxAlgorithm
          // 
          this.comboBoxAlgorithm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.comboBoxAlgorithm.FormattingEnabled = true;
          this.comboBoxAlgorithm.Items.AddRange(new object[] {
            "MD5",
            "SHA1",
            "SHA256",
            "SHA384",
            "SHA512"});
          this.comboBoxAlgorithm.Location = new System.Drawing.Point(6, 35);
          this.comboBoxAlgorithm.Name = "comboBoxAlgorithm";
          this.comboBoxAlgorithm.Size = new System.Drawing.Size(106, 21);
          this.comboBoxAlgorithm.TabIndex = 0;
          this.comboBoxAlgorithm.SelectedIndexChanged += new System.EventHandler(this.comboBoxAlgorithm_SelectedIndexChanged);
          // 
          // textBoxSourceText
          // 
          this.textBoxSourceText.Location = new System.Drawing.Point(85, 309);
          this.textBoxSourceText.Name = "textBoxSourceText";
          this.textBoxSourceText.Size = new System.Drawing.Size(222, 20);
          this.textBoxSourceText.TabIndex = 6;
          this.textBoxSourceText.TextChanged += new System.EventHandler(this.textBoxSourceText_TextChanged);
          // 
          // groupBoxIdenticonPreview
          // 
          this.groupBoxIdenticonPreview.Controls.Add(this.pictureBoxIdenticon);
          this.groupBoxIdenticonPreview.Location = new System.Drawing.Point(127, 29);
          this.groupBoxIdenticonPreview.Name = "groupBoxIdenticonPreview";
          this.groupBoxIdenticonPreview.Size = new System.Drawing.Size(262, 275);
          this.groupBoxIdenticonPreview.TabIndex = 2;
          this.groupBoxIdenticonPreview.TabStop = false;
          this.groupBoxIdenticonPreview.Text = "Identicon preview";
          // 
          // pictureBoxIdenticon
          // 
          this.pictureBoxIdenticon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
          this.pictureBoxIdenticon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pictureBoxIdenticon.Location = new System.Drawing.Point(3, 16);
          this.pictureBoxIdenticon.Name = "pictureBoxIdenticon";
          this.pictureBoxIdenticon.Size = new System.Drawing.Size(256, 256);
          this.pictureBoxIdenticon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
          this.pictureBoxIdenticon.TabIndex = 0;
          this.pictureBoxIdenticon.TabStop = false;
          // 
          // buttonGenerate
          // 
          this.buttonGenerate.Location = new System.Drawing.Point(311, 308);
          this.buttonGenerate.Name = "buttonGenerate";
          this.buttonGenerate.Size = new System.Drawing.Size(78, 22);
          this.buttonGenerate.TabIndex = 7;
          this.buttonGenerate.Text = "Generate";
          this.buttonGenerate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
          this.buttonGenerate.UseVisualStyleBackColor = true;
          this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
          // 
          // saveFileDialogIdenticon
          // 
          this.saveFileDialogIdenticon.FileName = "identicon.png";
          this.saveFileDialogIdenticon.Filter = "PNG files|*.png|BMP files|*.bmp|JPEG files|*.jpg|ICO files|*.ico";
          this.saveFileDialogIdenticon.Title = "Save Identicon as...";
          this.saveFileDialogIdenticon.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialogIdenticon_FileOk);
          // 
          // labelTextInfo
          // 
          this.labelTextInfo.AutoSize = true;
          this.labelTextInfo.Location = new System.Drawing.Point(1, 312);
          this.labelTextInfo.Name = "labelTextInfo";
          this.labelTextInfo.Size = new System.Drawing.Size(76, 13);
          this.labelTextInfo.TabIndex = 8;
          this.labelTextInfo.Text = "Text to iconify:";
          // 
          // frmMain
          // 
          this.AcceptButton = this.buttonGenerate;
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(393, 335);
          this.Controls.Add(this.labelTextInfo);
          this.Controls.Add(this.buttonGenerate);
          this.Controls.Add(this.groupBoxIdenticonPreview);
          this.Controls.Add(this.groupBoxProperties);
          this.Controls.Add(this.menuStripMain);
          this.Controls.Add(this.textBoxSourceText);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MainMenuStrip = this.menuStripMain;
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "frmMain";
          this.Text = "Identicon Generator";
          this.Load += new System.EventHandler(this.frmMain_Load);
          this.menuStripMain.ResumeLayout(false);
          this.menuStripMain.PerformLayout();
          this.groupBoxProperties.ResumeLayout(false);
          this.groupBoxProperties.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxForegroundColorPreview)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBackgroundColorPreview)).EndInit();
          this.groupBoxBlocks.ResumeLayout(false);
          this.groupBoxBlocks.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlocksVertical)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlocksHorizontal)).EndInit();
          this.groupBoxSize.ResumeLayout(false);
          this.groupBoxSize.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHeight)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWidth)).EndInit();
          this.groupBoxIdenticonPreview.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIdenticon)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveIdenticonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxProperties;
        private System.Windows.Forms.GroupBox groupBoxIdenticonPreview;
        private System.Windows.Forms.PictureBox pictureBoxIdenticon;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.ComboBox comboBoxAlgorithm;
        private System.Windows.Forms.Label labelAlgorithm;
        private System.Windows.Forms.GroupBox groupBoxSize;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.GroupBox groupBoxBlocks;
        private System.Windows.Forms.Label labelVert;
        private System.Windows.Forms.Label labelHoriz;
        private System.Windows.Forms.TextBox textBoxSourceText;
        private System.Windows.Forms.Button buttonBackgroundColor;
        private System.Windows.Forms.ColorDialog colorDialogBackground;
        private System.Windows.Forms.PictureBox pictureBoxBackgroundColorPreview;
        private System.Windows.Forms.NumericUpDown numericUpDownBlocksVertical;
        private System.Windows.Forms.NumericUpDown numericUpDownBlocksHorizontal;
        private System.Windows.Forms.NumericUpDown numericUpDownHeight;
        private System.Windows.Forms.NumericUpDown numericUpDownWidth;
        private System.Windows.Forms.SaveFileDialog saveFileDialogIdenticon;
        private System.Windows.Forms.CheckBox checkBoxBackgroundTransparent;
        private System.Windows.Forms.CheckBox checkBoxColorFromText;
        private System.Windows.Forms.PictureBox pictureBoxForegroundColorPreview;
        private System.Windows.Forms.Button buttonForegroundColor;
        private System.Windows.Forms.ColorDialog colorDialogForeground;
        private System.Windows.Forms.Label labelTextInfo;
        private System.Windows.Forms.ToolStripMenuItem selectlanguageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transformToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate90DegCCWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate90DegCWToolStripMenuItem;

    }
}


﻿using IdenticonLib;
using LocaleLib;
using SettingsLib;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace IdenticonGeneration {
  public partial class frmMain : Form {
    // Private fields accessors.
    #region Fields and Accessors
    private LocaleManager localeManager;
    private Settings settings;

    private Color backgroundIdenticonColor = Color.White;
    private Color foregroundIdenticonColor = Color.Green;

    private string lastKnownIdentitext = "";
    private float rotationAngle = 0;

    private bool IsSaveable {
      set {
        saveIdenticonToolStripMenuItem.Enabled = value;
        transformToolStripMenuItem.Enabled = value;
        if(value == false) {
          this.Text = localeManager.GetLocalizedString("{WINDOWTITLE_NONSAVEABLE}");
        } else {
          this.Text = localeManager.GetLocalizedString("{WINDOWTITLE_SAVEABLE}");
        }
      }
    }
    #endregion

    #region Constructor
    public frmMain() {
      InitializeComponent();

      settings = new Settings(".\\settings\\settings.xml");
      localeManager = new LocaleManager(".\\language", settings["activeLanguage"]);
    }
    #endregion

    // Things to do when I'll stop being a lazy bastard.
    #region TODO:
    /* * * * * * * * * * * * * * * * * * * * * *
     * Priority: The less, the more important.
     *
     * TODO: Random-colored block generation.
     *       PRIORITY: When I'll stop being a
     *                 lazy bastard.
     *       
     * TODO: SVG generation.
     *       PRIORITY: When I'll stop being a
     *                 lazy bastard.
     * * * * * * * * * * * * * * * * * * * * * */
    #endregion

    // Private methods for local use.
    #region Private methods
    private void SetUpLocale() {
      fileToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_FILE}");
      saveIdenticonToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_FILE_SAVEICON}");
      selectlanguageToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_FILE_LANGSELECT}");
      exitToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_FILE_EXIT}");

      transformToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_TRANSFORM}");
      rotate90DegCCWToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_TRANSFORM_ROTATE90CCW}");
      rotate90DegCWToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_TRANSFORM_ROTATE90CW}");

      helpToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_HELP}");
      aboutToolStripMenuItem.Text = localeManager.GetLocalizedString("{MENUSTRIP_HELP_ABOUT}");

      groupBoxProperties.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES}");
      labelAlgorithm.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_ALGORITHM}");

      groupBoxSize.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_SIZE}");
      labelWidth.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_SIZEWIDTH}");
      labelHeight.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_SIZEHEIGHT}");

      groupBoxBlocks.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_BLOCKS}");
      labelHoriz.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_BLOCKSHORIZONTAL}");
      labelVert.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_BLOCKSVERTICAL}");

      buttonBackgroundColor.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_BUTTONBACKGROUND}");
      checkBoxBackgroundTransparent.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_TRANSPARENT_BG}");

      buttonForegroundColor.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_BUTTONFOREGROUND}");
      checkBoxColorFromText.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPROPERTIES_FGCOLORFROMTEXT}");

      groupBoxIdenticonPreview.Text = localeManager.GetLocalizedString("{GROUPBOX_IDENTICONPREVIEW}");

      labelTextInfo.Text = localeManager.GetLocalizedString("{TEXTTOICONIFY}");
      buttonGenerate.Text = localeManager.GetLocalizedString("{BUTTON_GENERATE}");

      saveFileDialogIdenticon.Title = localeManager.GetLocalizedString("{SAVEFILEDIALOG_TITLE}");
    }
#endregion

    // Events fired by this form itself.
    #region Main form events
    private void frmMain_Load(object sender, EventArgs e) {
      // Select default algorithm.
      comboBoxAlgorithm.SelectedIndex = 0;

      // Set background color preview.
      pictureBoxBackgroundColorPreview.BackColor = backgroundIdenticonColor;

      // Set foreground color preview.
      pictureBoxForegroundColorPreview.BackColor = foregroundIdenticonColor;

      // Set saveable state.
      IsSaveable = false;

      // Set limits for numericUpDown controls.
      numericUpDownBlocksHorizontal.Maximum = numericUpDownHeight.Value;
      numericUpDownBlocksVertical.Maximum = numericUpDownHeight.Value;

      // Set localization.
      SetUpLocale();
    }

    #endregion

    // Events fired by main menu strip at the top of the window.
    #region ToolStrip events
    private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
      Environment.Exit(0);
    }

    private void saveIdenticonToolStripMenuItem_Click(object sender, EventArgs e) {
      saveFileDialogIdenticon.ShowDialog(this);
    }

    private void aboutToolStripMenuItem1_Click(object sender, EventArgs e) {
      frmAbout about = new frmAbout(localeManager.ActiveLocale);
      about.ShowDialog(this);
    }

    private void selectlanguageToolStripMenuItem_Click(object sender, EventArgs e) {
      frmLang frmlng = new frmLang(localeManager);
      frmlng.ShowDialog(this);
      SetUpLocale();
      settings["activeLanguage"] = localeManager.ActiveLocale;
    }
    private void rotate90DegCCWToolStripMenuItem_Click(object sender, EventArgs e) {
      pictureBoxIdenticon.Image = Renderer.RotateImage(pictureBoxIdenticon.Image, new PointF(pictureBoxIdenticon.Image.Width / 2, pictureBoxIdenticon.Image.Height / 2), -90);
      if (rotationAngle < -360) {
        rotationAngle = 0;
      } else {
        rotationAngle -= 90;
      }
    }

    private void rotate90DegCWToolStripMenuItem_Click(object sender, EventArgs e) {
      pictureBoxIdenticon.Image = Renderer.RotateImage(pictureBoxIdenticon.Image, new PointF(pictureBoxIdenticon.Image.Width / 2, pictureBoxIdenticon.Image.Height / 2), 90);
      if (rotationAngle > 360) {
        rotationAngle = 0;
      } else {
        rotationAngle += 90;
      }
    }
    #endregion

    // Events fired by image generation button.
    #region Button events
    private void buttonGenerate_Click(object sender, EventArgs e) {
      if (numericUpDownHeight.Value > pictureBoxIdenticon.Height || numericUpDownHeight.Value > pictureBoxIdenticon.Width) {
        pictureBoxIdenticon.SizeMode = PictureBoxSizeMode.StretchImage;
      } else {
        pictureBoxIdenticon.SizeMode = PictureBoxSizeMode.CenterImage;
      }

      try {
        if (checkBoxColorFromText.Checked) {
          pictureBoxIdenticon.Image =
            Renderer.RenderToBitmap(
              textBoxSourceText.Text,
              comboBoxAlgorithm.Text,
              new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
              backgroundIdenticonColor,
              new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
        } else {
          pictureBoxIdenticon.Image =
            Renderer.RenderToBitmap(
              textBoxSourceText.Text,
              comboBoxAlgorithm.Text,
              new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
              backgroundIdenticonColor,
              foregroundIdenticonColor,
              new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));              
        }

        lastKnownIdentitext = textBoxSourceText.Text;
        IsSaveable = true;
      } catch (Exception ex) {
        MessageBox.Show(this, string.Format("{0}\n {1}", localeManager.GetLocalizedString("{MESSAGEBOX_GENERATIONERROR}"), ex), "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      rotationAngle = 0;
    }

    private void buttonBackgroundColor_Click(object sender, EventArgs e) {
      DialogResult rslt = colorDialogBackground.ShowDialog(this);

      if (rslt == DialogResult.OK) {
        if (colorDialogBackground.Color != null) {
          backgroundIdenticonColor = colorDialogBackground.Color;
          pictureBoxBackgroundColorPreview.BackColor = backgroundIdenticonColor;
          IsSaveable = false;
        } else {
          backgroundIdenticonColor = Color.White;
          pictureBoxBackgroundColorPreview.BackColor = backgroundIdenticonColor;
        }
      }
    }

    private void buttonForegroundColor_Click(object sender, EventArgs e) {
      DialogResult rslt = colorDialogForeground.ShowDialog(this);

      if (rslt == DialogResult.OK) {
        if (colorDialogForeground.Color != null) {
          foregroundIdenticonColor = colorDialogForeground.Color;
          pictureBoxForegroundColorPreview.BackColor = foregroundIdenticonColor;
          IsSaveable = false;
        } else {
          foregroundIdenticonColor = Color.Green;
          pictureBoxForegroundColorPreview.BackColor = foregroundIdenticonColor;
        }
      }
    }
    #endregion

    // Events fired by Size and Block Amount numeric rotators.
    #region NumericUpDown events
    int prevHorizontalNumericValue = 0;
    private void numericUpDownBlocksHorizontal_ValueChanged(object sender, EventArgs e) {
      if (((int)numericUpDownBlocksHorizontal.Value % 2) != 0) {
        if (numericUpDownBlocksHorizontal.Value > prevHorizontalNumericValue) {
          if (numericUpDownBlocksHorizontal.Value + 2 <= numericUpDownBlocksHorizontal.Maximum) {
            numericUpDownBlocksHorizontal.Value++;
          } else {
            numericUpDownBlocksHorizontal.Value--;
          }
        }
        if (numericUpDownBlocksHorizontal.Value < prevHorizontalNumericValue) {
          numericUpDownBlocksHorizontal.Value--;
        }

        if ((int)numericUpDownBlocksHorizontal.Value > (int)numericUpDownWidth.Value) {
          numericUpDownBlocksHorizontal.Value = numericUpDownWidth.Value;
        }
      }
      prevHorizontalNumericValue = (int)numericUpDownBlocksHorizontal.Value;
      IsSaveable = false;
    }

    private void numericUpDownBlocksVertical_ValueChanged(object sender, EventArgs e) {
      if ((int)numericUpDownBlocksVertical.Value > (int)numericUpDownHeight.Value) {
        numericUpDownBlocksVertical.Value = numericUpDownHeight.Value;
      }
      IsSaveable = false;
    }

    private void numericUpDownWidth_ValueChanged(object sender, EventArgs e) {
      numericUpDownBlocksHorizontal.Maximum = numericUpDownWidth.Value;
      IsSaveable = false;
    }

    private void numericUpDownHeight_ValueChanged(object sender, EventArgs e) {
      numericUpDownBlocksVertical.Maximum = numericUpDownHeight.Value;
      IsSaveable = false;
    }
    #endregion

    // Events fired by image saving dialog.
    #region SaveFileDialog events
    private void saveFileDialogIdenticon_FileOk(object sender, CancelEventArgs e) {
      try {
        if (saveFileDialogIdenticon.FileName.EndsWith(".png")) {
          if (checkBoxColorFromText.Checked) {
            Bitmap bmp = Renderer.RenderToBitmap(
              textBoxSourceText.Text,
              comboBoxAlgorithm.Text,
              new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
              backgroundIdenticonColor,
              new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
            Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
              .Save(saveFileDialogIdenticon.FileName, ImageFormat.Png);

          } else {
            Bitmap bmp = Renderer.RenderToBitmap(
               textBoxSourceText.Text,
               comboBoxAlgorithm.Text,
               new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
               backgroundIdenticonColor,
               foregroundIdenticonColor,
               new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
            Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
              .Save(saveFileDialogIdenticon.FileName, ImageFormat.Png);
          }
        } else if (saveFileDialogIdenticon.FileName.EndsWith(".jpg")) {
          if (checkBoxBackgroundTransparent.Checked) {
            DialogResult drslt = MessageBox.Show(this, localeManager.GetLocalizedString("{MESSAGEBOX_TRANSPARENCYJPEG}"), "Hmmm...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drslt == System.Windows.Forms.DialogResult.Yes) {
              Bitmap bmp = Renderer.RenderToBitmap(
                textBoxSourceText.Text,
                comboBoxAlgorithm.Text,
                new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                Color.White,
                new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
              Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
                .Save(saveFileDialogIdenticon.FileName, ImageFormat.Jpeg);
            } else {
              return;
            }
          } else {
            if (checkBoxColorFromText.Checked) {
              Bitmap bmp = Renderer.RenderToBitmap(
                textBoxSourceText.Text,
                comboBoxAlgorithm.Text,
                new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                backgroundIdenticonColor,
                new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
              Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
                .Save(saveFileDialogIdenticon.FileName, ImageFormat.Jpeg);
            } else {
              Bitmap bmp = Renderer.RenderToBitmap(
                 textBoxSourceText.Text,
                 comboBoxAlgorithm.Text,
                 new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                 backgroundIdenticonColor,
                 foregroundIdenticonColor,
                 new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
              Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
                .Save(saveFileDialogIdenticon.FileName, ImageFormat.Jpeg);
            }
          }
        } else if (saveFileDialogIdenticon.FileName.EndsWith(".bmp")) {
          if (checkBoxBackgroundTransparent.Checked) {
            DialogResult drslt = MessageBox.Show(this, localeManager.GetLocalizedString("{MESSAGEBOX_TRANSPARENCYBMP}"), "Hmmm...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drslt == System.Windows.Forms.DialogResult.Yes) {
              Bitmap bmp = Renderer.RenderToBitmap(
                textBoxSourceText.Text,
                comboBoxAlgorithm.Text,
                new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                Color.White,
                new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
              Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
                .Save(saveFileDialogIdenticon.FileName, ImageFormat.Bmp);
            } else {
              return;
            }
          } else {
            if (checkBoxColorFromText.Checked) {
              Bitmap bmp = Renderer.RenderToBitmap(
                textBoxSourceText.Text,
                comboBoxAlgorithm.Text,
                new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                backgroundIdenticonColor,
                new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
              Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height/ 2), rotationAngle)
                .Save(saveFileDialogIdenticon.FileName, ImageFormat.Bmp);
            } else {
              Bitmap bmp = Renderer.RenderToBitmap(
                textBoxSourceText.Text,
                comboBoxAlgorithm.Text,
                new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                backgroundIdenticonColor,
                foregroundIdenticonColor,
                new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
              Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle)
                .Save(saveFileDialogIdenticon.FileName, ImageFormat.Bmp);
            }
          }
        } else if (saveFileDialogIdenticon.FileName.EndsWith(".ico")) {
          if (numericUpDownHeight.Value == numericUpDownWidth.Value) {
            if ((int)numericUpDownHeight.Value < 256 && (int)numericUpDownWidth.Value < 256) {
              Icon ico;
              if (checkBoxColorFromText.Checked) {
                Bitmap bmp = Renderer.RenderToBitmap(
                      textBoxSourceText.Text,
                      comboBoxAlgorithm.Text,
                      new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                      backgroundIdenticonColor,
                      new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
                bmp = Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle);
                ico = Renderer.GenerateIcon(bmp, (int)numericUpDownHeight.Value, true);
              } else {
                Bitmap bmp = Renderer.RenderToBitmap(
                    textBoxSourceText.Text,
                    comboBoxAlgorithm.Text,
                    new Size((int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value),
                    backgroundIdenticonColor,
                    foregroundIdenticonColor,
                    new Size((int)numericUpDownBlocksHorizontal.Value, (int)numericUpDownBlocksVertical.Value));
                bmp = Renderer.RotateImage(bmp, new PointF(bmp.Width / 2, bmp.Height / 2), rotationAngle);
                ico = Renderer.GenerateIcon(bmp, (int)numericUpDownHeight.Value, true);
              }

              using (StreamWriter sw = new StreamWriter(saveFileDialogIdenticon.FileName)) {
                ico.Save(sw.BaseStream);
                sw.Close();
                sw.Dispose();
              }
              Renderer.DestroyIcon(ico.Handle);
            } else {
              MessageBox.Show(this, localeManager.GetLocalizedString("{MESSAGEBOX_ICONSIZETOOLARGE}"), "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
          } else {
            MessageBox.Show(this, localeManager.GetLocalizedString("{MESSAGEBOX_ICONSIZENOTEQUAL}"), "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
          }
        } else {
          MessageBox.Show(this, localeManager.GetLocalizedString("{MESSAGEBOX_UNSUPPORTEDFILE}"), "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
      } catch (Exception ex) {
        MessageBox.Show(this, string.Format("{0}\n {1}", localeManager.GetLocalizedString("{MESSAGEBOX_GENERATIONERROR}"), ex), "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
    #endregion
    
    // Events fired by Background and Foreground CheckBoxes.
    #region CheckBox events
    private Color lastBackgroundKnownColor;
    private void checkBoxBackgroundTransparent_CheckedChanged(object sender, EventArgs e) {
      if (checkBoxBackgroundTransparent.Checked) {
        buttonBackgroundColor.Enabled = false;
        lastBackgroundKnownColor = pictureBoxBackgroundColorPreview.BackColor;
        pictureBoxBackgroundColorPreview.BackColor = Color.FromKnownColor(KnownColor.Transparent);
        backgroundIdenticonColor = Color.FromKnownColor(KnownColor.Transparent);
      } else {
        buttonBackgroundColor.Enabled = true;
        pictureBoxBackgroundColorPreview.BackColor = lastBackgroundKnownColor;
        backgroundIdenticonColor = lastBackgroundKnownColor;
      }
      IsSaveable = false;
    }

    private Color lastKnownForegroundColor;
    private void checkBoxColorFromText_CheckedChanged(object sender, EventArgs e) {
      if (checkBoxColorFromText.Checked) {
        buttonForegroundColor.Enabled = false;
        lastKnownForegroundColor = pictureBoxForegroundColorPreview.BackColor;
        pictureBoxForegroundColorPreview.BackColor = Color.FromKnownColor(KnownColor.Control);
      } else {
        buttonForegroundColor.Enabled = true;
        pictureBoxForegroundColorPreview.BackColor = lastKnownForegroundColor;
        
      }
      IsSaveable = false;
    }
    #endregion

    // Events fired by text input TextBox.
    #region TextBox events
    private void textBoxSourceText_TextChanged(object sender, EventArgs e) {
      if (textBoxSourceText.Text != lastKnownIdentitext) {
        IsSaveable = false;
      } else {
        IsSaveable = true;
      }
    }
    #endregion

    // Events fired by algorithm selection ComboBox.
    #region ComboBox events
    private void comboBoxAlgorithm_SelectedIndexChanged(object sender, EventArgs e) {
      IsSaveable = false;
    }
    #endregion
  }
}

﻿using LocaleLib;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IdenticonLib {
  public partial class frmLang : Form {
    private LocaleManager localeMan;
    public frmLang(LocaleManager localeMan) {
      InitializeComponent();
      this.localeMan = localeMan;
    }

    private void frmLang_Load(object sender, EventArgs e) {
      labelLanguage.Text = localeMan.GetLocalizedString("{LANGUAGECHOOSER_LANGUAGE}");
      buttonCancel.Text = localeMan.GetLocalizedString("{LANGUAGECHOOSER_CANCEL}");
      this.Text = localeMan.GetLocalizedString("{LANGUAGECHOOSER_TITLE}");

      foreach (KeyValuePair<string, Locale> kvp in localeMan.AvailableLocales) {
        comboBoxLanguageList.Items.Add(kvp.Value.FriendlyName);
      }
      comboBoxLanguageList.Text = localeMan.ActiveLocaleFriendlyName;
    }

    private void buttonOk_Click(object sender, EventArgs e) {
      localeMan.ActiveLocale = comboBoxLanguageList.Text;
      this.Close();
    }

    private void buttonClose_Click(object sender, EventArgs e) {
      this.Close();
    }
  }
}

﻿using IdenticonLib.BrushGenerators;

using System;
using System.Drawing;

namespace IdenticonLib {
  public class Renderer {
    // Methods for use by every class.
    #region Public methods
    /// <summary>
    /// Renders a bitmap image.
    /// </summary>
    /// <param name="source">Source text to squash into a bitmap.</param>
    /// <param name="algorithm">Preferred generation algorithm.</param>
    /// <param name="size">Image size.</param>
    /// <param name="bgColor">Background color.</param>
    /// <param name="blocks">Number of blocks to generate.</param>
    /// <returns>Generated bitmap.</returns>
    public static Bitmap RenderToBitmap(string source, string algorithm, Size size, Color bgColor, Size blocks) {
      return new IdenticonGenerator(algorithm)
        .WithSize(size)
        .WithBackgroundColor(bgColor)
        .WithBlocks(blocks)
        .WithBlockGenerators(IdenticonGenerator.ExtendedBlockGeneratorsConfig)
        .WithBrushGenerator(new StaticColorBrushGenerator(StaticColorBrushGenerator.ColorFromText(source)))
        .Create(source);
    }

    /// <summary>
    /// Generates a bitmap with specified background color.
    /// </summary>
    /// <param name="source">Source text to squash into a bitmap.</param>
    /// <param name="algorithm">Preferred generation algorithm.</param>
    /// <param name="size">Image size.</param>
    /// <param name="bgColor">Background color.</param>
    /// <param name="fgColor">Foreground color.</param>
    /// <param name="blocks">Number of blocks to generate.</param>
    /// <returns>Generated bitmap.</returns>
    public static Bitmap RenderToBitmap(string source, string algorithm, Size size, Color bgColor, Color fgColor, Size blocks) {
      return new IdenticonGenerator(algorithm)
        .WithSize(size)
        .WithBackgroundColor(bgColor)
        .WithBlocks(blocks)
        .WithBlockGenerators(IdenticonGenerator.ExtendedBlockGeneratorsConfig)
        .WithBrushGenerator(new StaticColorBrushGenerator(fgColor))
        .Create(source);
    }

    /// <summary>
    /// Rotates an image.
    /// </summary>
    /// <param name="image">Image to rotate.</param>
    /// <param name="offset">Rotation offset.</param>
    /// <param name="angle">Rotation angle.</param>
    /// <returns></returns>
    public static Bitmap RotateImage(Image image, PointF offset, float angle) {
      if (image == null) {
        return null;
      }

      Bitmap rotated = new Bitmap(image.Width, image.Height);
      rotated.SetResolution(image.HorizontalResolution, image.VerticalResolution);

      Graphics g = Graphics.FromImage(rotated);
      g.TranslateTransform(offset.X, offset.Y);
      g.RotateTransform(angle);
      g.TranslateTransform(-offset.X, -offset.Y);
      g.DrawImage(image, new PointF(0, 0));

      return rotated;
    }
    
    /// <summary>
    /// Converts an image into an icon.
    /// </summary>
    /// <param name="img">The image that shall become an icon.</param>
    /// <param name="size">The width and height of the icon. Standard
    /// sizes are 16, 32, 48, 64, 128 and 256.</param>
    /// <param name="keepAspectRatio">Whether the image should be squashed into a
    /// square or whether whitespace should be put around it.</param>
    /// <returns>Converted Icon object.</returns>
    public static Icon GenerateIcon(Image img, int size, bool keepAspectRatio) {
      Bitmap square = new Bitmap(size, size);
      Graphics g = Graphics.FromImage(square);

      int x, y, w, h;

      if(!keepAspectRatio || img.Height == img.Width) {
        x = y = 0;
        w = h = size;
      } else {
        float r = (float)img.Width / (float)img.Height;

        if(r > 1) {
          w = size;
          h = (int)((float)size / r);
          x = 0; y = (size - h) / 2;
        } else {
          w = (int)((float)size * r);
          h = size;
          y = 0; x = (size - w) / 2;
        }
      }

      // Make the image shrink proportionally.
      g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
      g.DrawImage(img, x, y, w, h);
      g.Flush();

      // Get icon handle and return it to the caller.
      return Icon.FromHandle(square.GetHicon());
    }
    #endregion

    // External, non-dotnet library imports.
    #region Un-managed DLL Imports
    /// <summary>
    /// Destroys created icon to prevent memory leak.
    /// </summary>
    /// <param name="handle">Icon handle.</param>
    /// <returns>True if icon existed.</returns>
    [System.Runtime.InteropServices.DllImport("user32.dll")]
    public extern static bool DestroyIcon(IntPtr handle);
    #endregion
  }
}

﻿using IdenticonLib.BrushGenerators;
using LocaleLib;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

namespace IdenticonLib {
  partial class frmAbout : Form {
    // Private fields and accessors.
    #region Fields
    private int identiconSeed;
    private LocaleManager localeMan;
    #endregion

    #region Constructor
    public frmAbout(string activeLocale) {
     InitializeComponent();
     identiconSeed = (int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
     localeMan = new LocaleManager(".\\language");
     localeMan.ActiveLocale = activeLocale;
    }
    #endregion

    // Functions for local use.
    #region Private functions
    private string GetVersionString() {
      Version vrs = Assembly.GetExecutingAssembly().GetName().Version;
      return string.Format("{0}.{1}.{2}", vrs.Major, vrs.Minor, vrs.Build);
    }

    private void SetUpLocale() {
      this.Text = localeMan.GetLocalizedString("{ABOUTBOX_TITLE}");
      labelExternLib.Text = localeMan.GetLocalizedString("{ABOUTBOX_USES_NIDENTICON}");
      labelVersion.Text = string.Format("{0} {1}", localeMan.GetLocalizedString("{ABOUTBOX_VERSION}"), GetVersionString());
      groupBoxCopyright.Text = localeMan.GetLocalizedString("{ABOUTBOX_GROUPBOX_AUTHOR}");
      groupBoxSpecialThanks.Text = localeMan.GetLocalizedString("{ABOUTBOX_GROUPBOX_SPECIALTHANKS}");
      labelTranslators.Text = localeMan.GetLocalizedString("{ABOUTBOX_GROUPBOX_SPECIALTHANKS_TRANSLATORS}");
      labelGreetings.Text = localeMan.GetLocalizedString("{ABOUTBOX_GROUPBOX_SPECIALTHANKS_GREETINGS}");
      labelHuh.Text = localeMan.GetLocalizedString("{PANCAKES}");
    }
    #endregion

    // Events fired by this form itself.
    #region Main form events
    private void frmAbout_Load(object sender, EventArgs e) {
      SetUpLocale();
      labelIdenticonSeed.Text = identiconSeed.ToString();

      pictureBoxIdenticon.Image = new IdenticonGenerator("MD5")
                            .WithSize(new Size(128, 128))
                            .WithBackgroundColor(Color.White)
                            .WithBlocks(new Size(8, 8))
                            .WithBlockGenerators(IdenticonGenerator.ExtendedBlockGeneratorsConfig)
                            .WithBrushGenerator(new StaticColorBrushGenerator(StaticColorBrushGenerator.ColorFromText(labelIdenticonSeed.Text)))
                            .Create(labelIdenticonSeed.Text);
    }
    #endregion

    // Events fired by link labels.
    #region LinkLabel events
    private void linkLabelCreditForHisAwesomeWork_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
      System.Diagnostics.Process.Start("https://github.com/RobThree/NIdenticon");
    }

    private void linkLabelMademan_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
      System.Diagnostics.Process.Start("http://mademan.xaa.pl/");
    }
    #endregion

    // Events fired by OK button.
    #region Button events
    private void buttonOK_Click(object sender, EventArgs e) {
      this.Close();
    }
    #endregion

    // Events fired by seed label button.
    #region Label events
    private void labelIdenticonSeed_MouseClick(object sender, MouseEventArgs e) {
      if(e.Button == System.Windows.Forms.MouseButtons.Right) {
        labelSeedContextMenuStrip.Show(labelIdenticonSeed, new Point(e.X, e.Y));
      }
    }
    #endregion

    // Events fired by context menu used for seed copying.
    #region ContextMenuStrip events
    private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e) {
      Clipboard.SetText(labelIdenticonSeed.Text);
    }
    #endregion
  }
}

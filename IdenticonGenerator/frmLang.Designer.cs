﻿namespace IdenticonLib {
  partial class frmLang {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.comboBoxLanguageList = new System.Windows.Forms.ComboBox();
      this.labelLanguage = new System.Windows.Forms.Label();
      this.buttonOk = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // comboBoxLanguageList
      // 
      this.comboBoxLanguageList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxLanguageList.FormattingEnabled = true;
      this.comboBoxLanguageList.Location = new System.Drawing.Point(5, 19);
      this.comboBoxLanguageList.Name = "comboBoxLanguageList";
      this.comboBoxLanguageList.Size = new System.Drawing.Size(183, 21);
      this.comboBoxLanguageList.TabIndex = 0;
      // 
      // labelLanguage
      // 
      this.labelLanguage.AutoSize = true;
      this.labelLanguage.Location = new System.Drawing.Point(4, 3);
      this.labelLanguage.Name = "labelLanguage";
      this.labelLanguage.Size = new System.Drawing.Size(93, 13);
      this.labelLanguage.TabIndex = 1;
      this.labelLanguage.Text = "Choose language:";
      // 
      // buttonOk
      // 
      this.buttonOk.Location = new System.Drawing.Point(114, 44);
      this.buttonOk.Name = "buttonOk";
      this.buttonOk.Size = new System.Drawing.Size(75, 23);
      this.buttonOk.TabIndex = 2;
      this.buttonOk.Text = "OK";
      this.buttonOk.UseVisualStyleBackColor = true;
      this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(35, 44);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 2;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonClose_Click);
      // 
      // frmLang
      // 
      this.AcceptButton = this.buttonOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(193, 72);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.buttonOk);
      this.Controls.Add(this.labelLanguage);
      this.Controls.Add(this.comboBoxLanguageList);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frmLang";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Language";
      this.Load += new System.EventHandler(this.frmLang_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox comboBoxLanguageList;
    private System.Windows.Forms.Label labelLanguage;
    private System.Windows.Forms.Button buttonOk;
    private System.Windows.Forms.Button buttonCancel;
  }
}
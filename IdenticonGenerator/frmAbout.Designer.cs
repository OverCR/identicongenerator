﻿namespace IdenticonLib {
    partial class frmAbout {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
          this.components = new System.ComponentModel.Container();
          this.buttonOK = new System.Windows.Forms.Button();
          this.pictureBoxIdenticon = new System.Windows.Forms.PictureBox();
          this.labelIdenticonSeed = new System.Windows.Forms.Label();
          this.labelProgName = new System.Windows.Forms.Label();
          this.linkLabelCreditForHisAwesomeWork = new System.Windows.Forms.LinkLabel();
          this.labelVersion = new System.Windows.Forms.Label();
          this.labelCopyright = new System.Windows.Forms.Label();
          this.labelExternLib = new System.Windows.Forms.Label();
          this.groupBoxCopyright = new System.Windows.Forms.GroupBox();
          this.groupBoxSpecialThanks = new System.Windows.Forms.GroupBox();
          this.labelGreetingsContent = new System.Windows.Forms.Label();
          this.labelPersonalUberCrashtester = new System.Windows.Forms.Label();
          this.linkLabelMademan = new System.Windows.Forms.LinkLabel();
          this.labelGerman = new System.Windows.Forms.Label();
          this.labelGreetings = new System.Windows.Forms.Label();
          this.labelTester = new System.Windows.Forms.Label();
          this.labelTranslators = new System.Windows.Forms.Label();
          this.labelHuh = new System.Windows.Forms.Label();
          this.labelSeedContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
          this.copyToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIdenticon)).BeginInit();
          this.groupBoxCopyright.SuspendLayout();
          this.groupBoxSpecialThanks.SuspendLayout();
          this.labelSeedContextMenuStrip.SuspendLayout();
          this.SuspendLayout();
          // 
          // buttonOK
          // 
          this.buttonOK.Location = new System.Drawing.Point(483, 137);
          this.buttonOK.Name = "buttonOK";
          this.buttonOK.Size = new System.Drawing.Size(75, 23);
          this.buttonOK.TabIndex = 0;
          this.buttonOK.Text = "OK";
          this.buttonOK.UseVisualStyleBackColor = true;
          this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
          // 
          // pictureBoxIdenticon
          // 
          this.pictureBoxIdenticon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pictureBoxIdenticon.Location = new System.Drawing.Point(5, 5);
          this.pictureBoxIdenticon.Name = "pictureBoxIdenticon";
          this.pictureBoxIdenticon.Size = new System.Drawing.Size(128, 128);
          this.pictureBoxIdenticon.TabIndex = 1;
          this.pictureBoxIdenticon.TabStop = false;
          // 
          // labelIdenticonSeed
          // 
          this.labelIdenticonSeed.BackColor = System.Drawing.Color.Transparent;
          this.labelIdenticonSeed.Location = new System.Drawing.Point(5, 134);
          this.labelIdenticonSeed.Name = "labelIdenticonSeed";
          this.labelIdenticonSeed.Size = new System.Drawing.Size(128, 15);
          this.labelIdenticonSeed.TabIndex = 2;
          this.labelIdenticonSeed.Text = "1390986794";
          this.labelIdenticonSeed.TextAlign = System.Drawing.ContentAlignment.TopCenter;
          this.labelIdenticonSeed.MouseClick += new System.Windows.Forms.MouseEventHandler(this.labelIdenticonSeed_MouseClick);
          // 
          // labelProgName
          // 
          this.labelProgName.AutoSize = true;
          this.labelProgName.Location = new System.Drawing.Point(7, 17);
          this.labelProgName.Name = "labelProgName";
          this.labelProgName.Size = new System.Drawing.Size(101, 13);
          this.labelProgName.TabIndex = 3;
          this.labelProgName.Text = "Identicon Generator";
          // 
          // linkLabelCreditForHisAwesomeWork
          // 
          this.linkLabelCreditForHisAwesomeWork.AutoSize = true;
          this.linkLabelCreditForHisAwesomeWork.Location = new System.Drawing.Point(6, 107);
          this.linkLabelCreditForHisAwesomeWork.Name = "linkLabelCreditForHisAwesomeWork";
          this.linkLabelCreditForHisAwesomeWork.Size = new System.Drawing.Size(205, 13);
          this.linkLabelCreditForHisAwesomeWork.TabIndex = 4;
          this.linkLabelCreditForHisAwesomeWork.TabStop = true;
          this.linkLabelCreditForHisAwesomeWork.Text = "https://github.com/RobThree/NIdenticon";
          this.linkLabelCreditForHisAwesomeWork.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelCreditForHisAwesomeWork_LinkClicked);
          // 
          // labelVersion
          // 
          this.labelVersion.AutoSize = true;
          this.labelVersion.Location = new System.Drawing.Point(7, 34);
          this.labelVersion.Name = "labelVersion";
          this.labelVersion.Size = new System.Drawing.Size(42, 13);
          this.labelVersion.TabIndex = 5;
          this.labelVersion.Text = "Version";
          // 
          // labelCopyright
          // 
          this.labelCopyright.AutoSize = true;
          this.labelCopyright.Location = new System.Drawing.Point(8, 51);
          this.labelCopyright.Name = "labelCopyright";
          this.labelCopyright.Size = new System.Drawing.Size(170, 13);
          this.labelCopyright.TabIndex = 6;
          this.labelCopyright.Text = "Copyright (C) 2014 Tomasz Cichoń";
          // 
          // labelExternLib
          // 
          this.labelExternLib.AutoSize = true;
          this.labelExternLib.Location = new System.Drawing.Point(4, 91);
          this.labelExternLib.Name = "labelExternLib";
          this.labelExternLib.Size = new System.Drawing.Size(210, 13);
          this.labelExternLib.TabIndex = 7;
          this.labelExternLib.Text = "This program uses modified NIdenticon Lib:";
          // 
          // groupBoxCopyright
          // 
          this.groupBoxCopyright.Controls.Add(this.labelExternLib);
          this.groupBoxCopyright.Controls.Add(this.labelProgName);
          this.groupBoxCopyright.Controls.Add(this.labelCopyright);
          this.groupBoxCopyright.Controls.Add(this.linkLabelCreditForHisAwesomeWork);
          this.groupBoxCopyright.Controls.Add(this.labelVersion);
          this.groupBoxCopyright.Location = new System.Drawing.Point(139, 5);
          this.groupBoxCopyright.Name = "groupBoxCopyright";
          this.groupBoxCopyright.Size = new System.Drawing.Size(219, 128);
          this.groupBoxCopyright.TabIndex = 8;
          this.groupBoxCopyright.TabStop = false;
          this.groupBoxCopyright.Text = "Author";
          // 
          // groupBoxSpecialThanks
          // 
          this.groupBoxSpecialThanks.Controls.Add(this.labelGreetingsContent);
          this.groupBoxSpecialThanks.Controls.Add(this.labelPersonalUberCrashtester);
          this.groupBoxSpecialThanks.Controls.Add(this.linkLabelMademan);
          this.groupBoxSpecialThanks.Controls.Add(this.labelGerman);
          this.groupBoxSpecialThanks.Controls.Add(this.labelGreetings);
          this.groupBoxSpecialThanks.Controls.Add(this.labelTester);
          this.groupBoxSpecialThanks.Controls.Add(this.labelTranslators);
          this.groupBoxSpecialThanks.Location = new System.Drawing.Point(364, 5);
          this.groupBoxSpecialThanks.Name = "groupBoxSpecialThanks";
          this.groupBoxSpecialThanks.Size = new System.Drawing.Size(194, 128);
          this.groupBoxSpecialThanks.TabIndex = 9;
          this.groupBoxSpecialThanks.TabStop = false;
          this.groupBoxSpecialThanks.Text = "Special thanks";
          // 
          // labelGreetingsContent
          // 
          this.labelGreetingsContent.AutoSize = true;
          this.labelGreetingsContent.Location = new System.Drawing.Point(12, 104);
          this.labelGreetingsContent.Name = "labelGreetingsContent";
          this.labelGreetingsContent.Size = new System.Drawing.Size(156, 13);
          this.labelGreetingsContent.TabIndex = 4;
          this.labelGreetingsContent.Text = "kadet1090, pisarz1958, Claudia\r\n";
          // 
          // labelPersonalUberCrashtester
          // 
          this.labelPersonalUberCrashtester.AutoSize = true;
          this.labelPersonalUberCrashtester.Location = new System.Drawing.Point(12, 68);
          this.labelPersonalUberCrashtester.Name = "labelPersonalUberCrashtester";
          this.labelPersonalUberCrashtester.Size = new System.Drawing.Size(176, 13);
          this.labelPersonalUberCrashtester.TabIndex = 3;
          this.labelPersonalUberCrashtester.Text = "Crashtester - Mateusz Wygrzywalski";
          // 
          // linkLabelMademan
          // 
          this.linkLabelMademan.AutoSize = true;
          this.linkLabelMademan.Location = new System.Drawing.Point(63, 33);
          this.linkLabelMademan.Name = "linkLabelMademan";
          this.linkLabelMademan.Size = new System.Drawing.Size(54, 13);
          this.linkLabelMademan.TabIndex = 2;
          this.linkLabelMademan.TabStop = true;
          this.linkLabelMademan.Text = "Mademan";
          this.linkLabelMademan.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMademan_LinkClicked);
          // 
          // labelGerman
          // 
          this.labelGerman.AutoSize = true;
          this.labelGerman.Location = new System.Drawing.Point(12, 33);
          this.labelGerman.Name = "labelGerman";
          this.labelGerman.Size = new System.Drawing.Size(53, 13);
          this.labelGerman.TabIndex = 1;
          this.labelGerman.Text = "Deutsch -";
          // 
          // labelGreetings
          // 
          this.labelGreetings.AutoSize = true;
          this.labelGreetings.Location = new System.Drawing.Point(7, 89);
          this.labelGreetings.Name = "labelGreetings";
          this.labelGreetings.Size = new System.Drawing.Size(70, 13);
          this.labelGreetings.TabIndex = 0;
          this.labelGreetings.Text = "Greetings for:";
          // 
          // labelTester
          // 
          this.labelTester.AutoSize = true;
          this.labelTester.Location = new System.Drawing.Point(7, 53);
          this.labelTester.Name = "labelTester";
          this.labelTester.Size = new System.Drawing.Size(40, 13);
          this.labelTester.TabIndex = 0;
          this.labelTester.Text = "Tester:";
          // 
          // labelTranslators
          // 
          this.labelTranslators.AutoSize = true;
          this.labelTranslators.Location = new System.Drawing.Point(7, 18);
          this.labelTranslators.Name = "labelTranslators";
          this.labelTranslators.Size = new System.Drawing.Size(62, 13);
          this.labelTranslators.TabIndex = 0;
          this.labelTranslators.Text = "Translators:";
          // 
          // labelHuh
          // 
          this.labelHuh.AutoSize = true;
          this.labelHuh.Location = new System.Drawing.Point(136, 142);
          this.labelHuh.Name = "labelHuh";
          this.labelHuh.Size = new System.Drawing.Size(263, 13);
          this.labelHuh.TabIndex = 10;
          this.labelHuh.Text = "Finally, my first finished project. Wee. I feel like a Boss.";
          // 
          // labelSeedContextMenuStrip
          // 
          this.labelSeedContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToClipboardToolStripMenuItem});
          this.labelSeedContextMenuStrip.Name = "labelSeedContextMenuStrip";
          this.labelSeedContextMenuStrip.Size = new System.Drawing.Size(170, 48);
          // 
          // copyToClipboardToolStripMenuItem
          // 
          this.copyToClipboardToolStripMenuItem.Name = "copyToClipboardToolStripMenuItem";
          this.copyToClipboardToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
          this.copyToClipboardToolStripMenuItem.Text = "&Copy to clipboard";
          this.copyToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyToClipboardToolStripMenuItem_Click);
          // 
          // frmAbout
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(563, 165);
          this.Controls.Add(this.labelHuh);
          this.Controls.Add(this.groupBoxSpecialThanks);
          this.Controls.Add(this.groupBoxCopyright);
          this.Controls.Add(this.labelIdenticonSeed);
          this.Controls.Add(this.pictureBoxIdenticon);
          this.Controls.Add(this.buttonOK);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "frmAbout";
          this.Padding = new System.Windows.Forms.Padding(9);
          this.ShowIcon = false;
          this.ShowInTaskbar = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "About Identicon Generator";
          this.Load += new System.EventHandler(this.frmAbout_Load);
          ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIdenticon)).EndInit();
          this.groupBoxCopyright.ResumeLayout(false);
          this.groupBoxCopyright.PerformLayout();
          this.groupBoxSpecialThanks.ResumeLayout(false);
          this.groupBoxSpecialThanks.PerformLayout();
          this.labelSeedContextMenuStrip.ResumeLayout(false);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.PictureBox pictureBoxIdenticon;
        private System.Windows.Forms.Label labelIdenticonSeed;
        private System.Windows.Forms.Label labelProgName;
        private System.Windows.Forms.LinkLabel linkLabelCreditForHisAwesomeWork;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelExternLib;
        private System.Windows.Forms.GroupBox groupBoxCopyright;
        private System.Windows.Forms.GroupBox groupBoxSpecialThanks;
        private System.Windows.Forms.LinkLabel linkLabelMademan;
        private System.Windows.Forms.Label labelGerman;
        private System.Windows.Forms.Label labelTranslators;
        private System.Windows.Forms.Label labelPersonalUberCrashtester;
        private System.Windows.Forms.Label labelTester;
        private System.Windows.Forms.Label labelGreetingsContent;
        private System.Windows.Forms.Label labelHuh;
        private System.Windows.Forms.Label labelGreetings;
        private System.Windows.Forms.ContextMenuStrip labelSeedContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyToClipboardToolStripMenuItem;

    }
}
